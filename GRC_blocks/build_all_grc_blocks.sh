#!/usr/bin/env bash
# Simple bash script to build all GRC blocks defined in subfolders of the current folder
clear

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

homepwd="$(pwd)"
grc_blocks="$(find . -maxdepth 1 -type d -iname gr'*')"

for block in ${grc_blocks}; do
    clear
    echo -e "\n For the block '${block}'...\n\n\n\n"
    cd "${block}"
    [ ! -d build ] && mkdir build
    ( cd build && cmake --config Debug ../ && make && sudo make install && sudo /sbin/ldconfig ) || exit 1
    cd "${homepwd}"
done

echo -e "\n\n\n\nDone !"
echo -e "See more online https://bitbucket.org/scee_ietr/malin-multi-arm-bandit-learning-for-iot-networks-with-grc.git"
