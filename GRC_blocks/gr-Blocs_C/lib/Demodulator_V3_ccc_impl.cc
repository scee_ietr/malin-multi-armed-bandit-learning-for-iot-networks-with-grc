/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "Demodulator_V3_ccc_impl.h"

// Threshold on norms of complex numbers before defining as zero
float THRESHOLD_ON_NORM = 0.0001;

// Turn this on by commenting (false) and uncommenting (true), for full debug output
// #define DOYOUWANTODEBUG (true)
#define DOYOUWANTODEBUG (false)

namespace gr {
    namespace Blocs_C {

        Demodulator_V3_ccc::sptr
        Demodulator_V3_ccc::make(
            const int channelNum,
            const int windowLength,
            std::vector<float> thresholds,
            bool moving
        ) {
        return gnuradio::get_initial_sptr
            (new Demodulator_V3_ccc_impl(
                channelNum,
                windowLength,
                thresholds,
                moving
            ));
        }

        /*
        * The private constructor
        */
        Demodulator_V3_ccc_impl::Demodulator_V3_ccc_impl(
            const int channelNum,
            int const windowLength,
            std::vector<float> thresholds,
            bool moving):
        gr::sync_block("Demodulator_V3_ccc",
            gr::io_signature::make(1, 1, channelNum * sizeof(gr_complex)),
            gr::io_signature::make(2, 2, channelNum * sizeof(gr_complex))
        ),
            d_channelNum(channelNum),
            d_windowLength(windowLength),
            d_thresholdsV(thresholds),
            d_thresholds(new float[thresholds.size()]),
            d_valuesArray(new float[channelNum * windowLength]),
            d_meanArray(new float[channelNum]),
            d_indexArray(new int[channelNum])
        {
            // set_output_multiple(128);  // FIXME

            std::cout << std::endl << "Creating a new 'Demodulator_V3_ccc_impl.cc' block..." << std::endl;  // DEBUG
            int output_mutiple = sizeof(gr_complex) * d_channelNum;
            std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
            set_output_multiple( output_mutiple );

            // DEBUG show input values of parameters
            std::cout << "d_channelNum: " << d_channelNum << std::endl;  // DEBUG
            std::cout << "d_windowLength: " << d_windowLength << std::endl;  // DEBUG

            for(int j = 0; j < d_channelNum; j++) {
                d_meanArray[j] = 0;
                d_indexArray[j] = 0;
                d_thresholds[j] = d_thresholdsV[j];
                std::cout << "    d_thresholds[" << j << "] = " << d_thresholds[j] << std::endl;  // DEBUG
            }
            for(int t = 0; t < d_channelNum*d_windowLength; t++) {
                d_valuesArray[t] = 0;
            }
            int currentIndex(0);
        }

        /*
        * Our virtual destructor.
        */
        Demodulator_V3_ccc_impl::~Demodulator_V3_ccc_impl() {
        }

        /*
        * Private method to project a gr_complex number
        */
        gr_complex Demodulator_V3_ccc_impl::projection(gr_complex z) {
            if (z == 0.0f) {
                return 0.0f;
            } else {
                if (real(z) > THRESHOLD_ON_NORM) {
                    if (imag(z) > THRESHOLD_ON_NORM) {
                        return 1+1j;
                    } else {
                        if (imag(z) < THRESHOLD_ON_NORM) {
                            return 1-1j;
                        }
                    }
                } else {
                    if (real(z) < THRESHOLD_ON_NORM) {
                        if (imag(z) > THRESHOLD_ON_NORM) {
                            return -1+1j;
                        } else {
                            if (imag(z) < THRESHOLD_ON_NORM) {
                                return -1-1j;
                            }
                        }
                    }
                }
            }
            return 0+0j;
        }

        /*
        * Signal processing
        */
        int Demodulator_V3_ccc_impl::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            // std::cout << "Start Demodulator_V3_ccc_impl::work()..." << std::endl;  // DEBUG

            const gr_complex *in = (const gr_complex *) input_items[0];
            gr_complex *out0 = (gr_complex *) output_items[0];
            gr_complex *out1 = (gr_complex *) output_items[1];

            int offset_in = 0;
            int offset_d  = 0;

            // Main loop
            for (int j = 0; j < d_channelNum; j++) {

                if (d_thresholds[j] != 0) {
                    for (int i = 0; i < noutput_items; i++) {
                        offset_in = i * d_channelNum + j;
                        offset_d = d_indexArray[j] * d_channelNum + j;

                        // XXX The shifting window is HERE
                        d_meanArray[j] += ( norm(in[offset_in]) - d_valuesArray[offset_d] ) / d_windowLength;
                        d_valuesArray[offset_d] = norm(in[offset_in]);
                        d_indexArray[j] = (d_indexArray[j] + 1) % d_windowLength;

                        if ( d_meanArray[j] >= d_thresholds[j] ) {
                            out0[offset_in] = projection(in[offset_in]);
                            // DEBUG
                            if (DOYOUWANTODEBUG) {
                                if (i == 0) {
                                    std::cout << "On channel " << j << " an input = " << in[offset_in] << " changed the current mean to " << d_meanArray[j] << " which is larger than threshold = " << d_thresholds[j] << " and so we output a projection of the input to " << out0[offset_in] << std::endl;
                                }
                            }
                        } else {
                            out0[offset_in] = 0+0j;
                        }
                        out1[offset_in] = d_meanArray[j];
                    }
                }
            }

            // Tell runtime system how many output items we produced.
            return noutput_items;
        }

    } /* namespace Blocs_C */
} /* namespace gr */

