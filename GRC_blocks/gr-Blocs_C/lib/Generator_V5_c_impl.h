/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_BLOCS_C_GENERATOR_V5_C_IMPL_H
#define INCLUDED_BLOCS_C_GENERATOR_V5_C_IMPL_H

#include <Blocs_C/Generator_V5_c.h>
#include <gnuradio/random.h>

namespace gr {
    namespace Blocs_C {

        class Generator_V5_c_impl : public Generator_V5_c
        {
            private:
                // Nothing to declare in this block.
                int d_Nb_ch;
                int d_preamLength;
                int d_dataLength;
                int d_totalLength;
                std::vector<int> d_activeChannels;
                int d_activeChannelNum;
                float d_gain;
                gr::random *d_rng;
                int* d_remainingTicks;
                float* d_proba;
                //gr_complex *d_out;

            public:
                Generator_V5_c_impl(
                    const int Nb_ch,
                    const std::vector<int> activeChannels,
                    const std::vector<float> occupancyRate,
                    const int preamLength,
                    const int dataLength,
                    const float digitalGain
                );
                ~Generator_V5_c_impl();

                // Where all the action really happens
                int work(
                    int noutput_items,
                    gr_vector_const_void_star &input_items,
                    gr_vector_void_star &output_items
                );

                int random_value_int();
                float random_value_1();
        };

    } // namespace Blocs_C
} // namespace gr

#endif /* INCLUDED_BLOCS_C_GENERATOR_V5_C_IMPL_H */

