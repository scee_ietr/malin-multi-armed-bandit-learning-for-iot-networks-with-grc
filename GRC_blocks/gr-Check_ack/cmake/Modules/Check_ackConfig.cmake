INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_CHECK_ACK Check_ack)

FIND_PATH(
    CHECK_ACK_INCLUDE_DIRS
    NAMES Check_ack/api.h
    HINTS $ENV{CHECK_ACK_DIR}/include
        ${PC_CHECK_ACK_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    CHECK_ACK_LIBRARIES
    NAMES gnuradio-Check_ack
    HINTS $ENV{CHECK_ACK_DIR}/lib
        ${PC_CHECK_ACK_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CHECK_ACK DEFAULT_MSG CHECK_ACK_LIBRARIES CHECK_ACK_INCLUDE_DIRS)
MARK_AS_ADVANCED(CHECK_ACK_LIBRARIES CHECK_ACK_INCLUDE_DIRS)

