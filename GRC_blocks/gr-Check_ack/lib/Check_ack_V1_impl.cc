/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "Check_ack_V1_impl.h"
#include <numeric>

namespace gr {
  namespace Check_ack {

    Check_ack_V1::sptr
    Check_ack_V1::make(const size_t Nb_channels,const size_t Packet_length)
    {
      return gnuradio::get_initial_sptr
        (new Check_ack_V1_impl(Nb_channels, Packet_length));
    }

    /*
     * The private constructor
     */
    Check_ack_V1_impl::Check_ack_V1_impl(const size_t Nb_channels,const size_t Packet_length)
      : gr::sync_block("Check_ack_V1",
              gr::io_signature::make(1, 1, Nb_channels* sizeof(gr_complex)),
              gr::io_signature::make(1, 1,Nb_channels* sizeof(unsigned char))),
		 	  d_Nb_channels(Nb_channels),
			  d_Packet_length(Packet_length),
		 	  d_real_part(Packet_length,0),
			  d_imag_part(Packet_length,0)
 		      //d_real_part(new float[Nb_channels * Packet_length]), // real part of the input
              //d_imag_part(new float[Nb_channels * Packet_length]) // imag part of the input
		 	  
    {
		set_output_multiple(128);

		std::cout << d_imag_part[0];

	}

    /*
     * Our virtual destructor.
     */
    Check_ack_V1_impl::~Check_ack_V1_impl()
    {
    }

    int
    Check_ack_V1_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      unsigned char *out = (unsigned char *) output_items[0];

      // Do <+signal processing+>
	  int sum_re 	= 0;
	  int sum_im 	= 0;

      // When a new buffer arrives, we isolate the imaginary and real parts
  	  for(int i=0;i<noutput_items;i++)
	  {
			for(int j=0;j<d_Nb_channels;j++)
			{
				// Add the new elements
				d_real_part.push_back((int) floor(real(in[d_Nb_channels*i+j])+0.5));
 				d_imag_part.push_back((int) floor(imag(in[d_Nb_channels*i+j])+0.5));
				// Erase the old elements
 				d_real_part.erase(d_real_part.begin());
 				d_imag_part.erase(d_imag_part.begin());

 				sum_re 			= std::accumulate(d_real_part.begin(),d_real_part.end(),0);
 				sum_im 			= std::accumulate(d_imag_part.begin(),d_imag_part.end(),0);
 				
 				// Debug
 				if(sum_re>300)
 				{
                                        std::cout << d_real_part.size() << std::endl;
                                }
				// verifying if the condition on the number of symbols equals to 1
				if((sum_re>0.98*d_Packet_length) && (sum_im>0.98*d_Packet_length))
				{
					out[d_Nb_channels*i+j] 		= 1;
					std::cout << j << std::endl;
				}
				else
				{
					out[d_Nb_channels*i+j] 		= 0;
				}

				
			}
	  }	  

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace Check_ack */
} /* namespace gr */

