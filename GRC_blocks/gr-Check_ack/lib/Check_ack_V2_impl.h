/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CHECK_ACK_CHECK_ACK_V2_IMPL_H
#define INCLUDED_CHECK_ACK_CHECK_ACK_V2_IMPL_H

#include <Check_ack/Check_ack_V2.h>

namespace gr {
    namespace Check_ack {
        class Check_ack_V2_impl : public Check_ack_V2 {
            private:
                // Nothing to declare in this block.
                const int   d_Nb_ch;
                const std::vector<int> d_activeChannels;
                const int   d_activeChannelNum;
                const int   d_Packet_l;
                const bool  d_is_gateway;
                const float d_r_data;
                const float d_i_data;
                int d_size_block_error;
                int d_size_detect_packet;
                int *d_count_ok;
                int *d_count_error;
                int *d_counter_start;
                int *d_count_ok_temp;
                int *d_data_char_acks;
                int *d_most_seen_char_acks;
                int *d_total_seen_char_acks;

            public:
                Check_ack_V2_impl(
                    const int   Nb_ch,
                    const std::vector<int> Active_ch,
                    const int   Packet_l,
                    const float Tot_err,
                    const float err_block,
                    const bool  is_gateway,
                    const float r_data,
                    const float i_data
                );
                ~Check_ack_V2_impl();

                // Where all the action really happens
                int work(
                    int noutput_items,
                    gr_vector_const_void_star &input_items,
                    gr_vector_void_star &output_items
                );
        };

    } // namespace Check_ack
} // namespace gr

#endif /* INCLUDED_CHECK_ACK_CHECK_ACK_V2_IMPL_H */
