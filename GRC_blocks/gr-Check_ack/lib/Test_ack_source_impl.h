/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CHECK_ACK_TEST_ACK_SOURCE_IMPL_H
#define INCLUDED_CHECK_ACK_TEST_ACK_SOURCE_IMPL_H

#include <Check_ack/Test_ack_source.h>
#include <gnuradio/random.h>

namespace gr {
  namespace Check_ack {

    class Test_ack_source_impl : public Test_ack_source
    {
     private:
      // Nothing to declare in this block.
      int d_Nb_ch;
      int d_Packet_l;
      int d_count;
      gr::random *d_rng;
      int d_channel_chosen;
     public:
      Test_ack_source_impl(int Nb_ch, int Packet_l);
      ~Test_ack_source_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
         
         int random_value_int();
    };

  } // namespace Check_ack
} // namespace gr

#endif /* INCLUDED_CHECK_ACK_TEST_ACK_SOURCE_IMPL_H */

