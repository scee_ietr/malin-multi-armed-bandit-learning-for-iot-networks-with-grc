/* -*- c++ -*- */

#define CHECK_ACK_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "Check_ack_swig_doc.i"

%{
#include "Check_ack/Check_ack_V1.h"
#include "Check_ack/Test_ack_source.h"
#include "Check_ack/Check_ack_V2.h"
%}


%include "Check_ack/Check_ack_V1.h"
GR_SWIG_BLOCK_MAGIC2(Check_ack, Check_ack_V1);
%include "Check_ack/Test_ack_source.h"
GR_SWIG_BLOCK_MAGIC2(Check_ack, Test_ack_source);
%include "Check_ack/Check_ack_V2.h"
GR_SWIG_BLOCK_MAGIC2(Check_ack, Check_ack_V2);
