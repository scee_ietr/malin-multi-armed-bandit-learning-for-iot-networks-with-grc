/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V3_H
#define INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V3_H

#include <Renormalize_ack/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
    namespace Renormalize_ack {

    /*!
        * \brief <+description of block+>
        * \ingroup Renormalize_ack
        *
        */
    class RENORMALIZE_ACK_API Renormalize_ack_v3 : virtual public gr::sync_block
    {
        public:
        typedef boost::shared_ptr<Renormalize_ack_v3> sptr;

        /*!
        * \brief Return a shared_ptr to a new instance of Renormalize_ack::Renormalize_ack_v3.
        *
        * To avoid accidental use of raw pointers, Renormalize_ack::Renormalize_ack_v3's
        * constructor is in a private implementation
        * class. Renormalize_ack::Renormalize_ack_v3::make is the public interface for
        * creating new instances.
        */
        static sptr make(const float Threshold, const int Nb_ch,const std::vector<int> Active_ch, const int Length_sum);
    };

    } // namespace Renormalize_ack
} // namespace gr

#endif /* INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V3_H */

