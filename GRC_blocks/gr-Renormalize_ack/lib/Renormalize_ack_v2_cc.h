/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CHECK_ACK_CHECK_ACK_V2_CC_H
#define INCLUDED_CHECK_ACK_CHECK_ACK_V2_CC_H

#include <Renormalize_ack/Renormalize_ack_v2.h>

namespace gr {
    namespace Renormalize_ack {

        class Renormalize_ack_v2_cc : public Renormalize_ack_v2 {
            private:
                // Nothing to declare in this block.
                const int d_Nb_ch;
                const std::vector<int> d_activeChannels;
                const int d_activeChannelNum;  // not an input
                const int d_Preamb_l;
                const int d_Data_l;
                const float d_Threshold_norm;
                // Declare here the internal variables!
                int  *d_current_data_count;
                int  *d_current_zero_data_count;
                bool *d_in_preamble;
                bool *d_in_message;
                int  *d_current_preamble_memory_offset;
                gr_complex *d_current_preamble_memory;
                // gr_complex *d_current_preamble_mean;
                gr_complex *d_multiplier;

            public:
                Renormalize_ack_v2_cc(
                    const int Nb_ch,
                    const std::vector<int> Active_ch,
                    const int Preamb_l,
                    const int Data_l,
                    const float Threshold_norm
                );
                ~Renormalize_ack_v2_cc();

                // Where all the action really happens
                int work(
                    int noutput_items,
                    gr_vector_const_void_star &input_items,
                    gr_vector_void_star &output_items
                );
        };

    } // namespace Renormalize_ack
} // namespace gr

#endif /* INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V2_CC_H */
