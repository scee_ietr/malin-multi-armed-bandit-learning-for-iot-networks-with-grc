/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V3_IMPL_H
#define INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V3_IMPL_H

#include <Renormalize_ack/Renormalize_ack_v3.h>
#include <complex>

namespace gr {
    namespace Renormalize_ack {

    class Renormalize_ack_v3_impl : public Renormalize_ack_v3
    {
        private:
            // Nothing to declare in this block.
            const float d_Threshold;
            const int d_Nb_ch;
            const std::vector<int> d_Active_ch;
            const int d_Length_sum;
            int d_count;
            gr_complex *d_multiplier;
            gr_complex *d_sum;
            int *d_state_ch;


        public:
            Renormalize_ack_v3_impl(
                const float Threshold,
                const int Nb_ch,
                const std::vector<int> Active_ch,
                const int Length_sum
            );
            ~Renormalize_ack_v3_impl();

        // Where all the action really happens
        int work(int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items);
    };

    } // namespace Renormalize_ack
} // namespace gr

#endif /* INCLUDED_RENORMALIZE_ACK_RENORMALIZE_ACK_V3_IMPL_H */

