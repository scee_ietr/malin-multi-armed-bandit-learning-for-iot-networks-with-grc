/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_SEND_ACK_SEND_ACK_V2_H
#define INCLUDED_SEND_ACK_SEND_ACK_V2_H

#include <Send_ack/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
    namespace Send_ack {

        /*!
        * \brief <+description of block+>
        * \ingroup Send_ack
        *
        */
        class SEND_ACK_API Send_ack_v2 : virtual public gr::sync_block
        {
            public:
            typedef boost::shared_ptr<Send_ack_v2> sptr;

            /*!
            * \brief Return a shared_ptr to a new instance of Send_ack::Send_ack_v2.
            *
            * To avoid accidental use of raw pointers, Send_ack::Send_ack_v2's
            * constructor is in a private implementation
            * class. Send_ack::Send_ack_v2::make is the public interface for
            * creating new instances.
            */
            static sptr make(
                const int Nb_ch,
                const std::vector<int> Active_ch,
                const int Delay_before_ack,
                const int Ack_length,
                const int Preamb_l
            );
        };

    } // namespace Send_ack
} // namespace gr

#endif /* INCLUDED_SEND_ACK_SEND_ACK_V2_H */

