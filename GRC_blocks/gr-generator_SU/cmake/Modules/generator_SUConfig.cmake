INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_GENERATOR_SU generator_SU)

FIND_PATH(
    GENERATOR_SU_INCLUDE_DIRS
    NAMES generator_SU/api.h
    HINTS $ENV{GENERATOR_SU_DIR}/include
        ${PC_GENERATOR_SU_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GENERATOR_SU_LIBRARIES
    NAMES gnuradio-generator_SU
    HINTS $ENV{GENERATOR_SU_DIR}/lib
        ${PC_GENERATOR_SU_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GENERATOR_SU DEFAULT_MSG GENERATOR_SU_LIBRARIES GENERATOR_SU_INCLUDE_DIRS)
MARK_AS_ADVANCED(GENERATOR_SU_LIBRARIES GENERATOR_SU_INCLUDE_DIRS)

