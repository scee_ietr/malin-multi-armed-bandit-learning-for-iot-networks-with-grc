/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_GENERATOR_SU_GENERATOR_SU_V2_H
#define INCLUDED_GENERATOR_SU_GENERATOR_SU_V2_H

#include <generator_SU/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace generator_SU {

    /*!
     * \brief <+description of block+>
     * \ingroup generator_SU
     *
     */
    class GENERATOR_SU_API generator_SU_V2 : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<generator_SU_V2> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of generator_SU::generator_SU_V2.
       *
       * To avoid accidental use of raw pointers, generator_SU::generator_SU_V2's
       * constructor is in a private implementation
       * class. generator_SU::generator_SU_V2::make is the public interface for
       * creating new instances.
       */
      static sptr make(int Nb_ch,std::vector<int> Active_ch,int Preamb_l,int data_l);
    };

  } // namespace generator_SU
} // namespace gr

#endif /* INCLUDED_GENERATOR_SU_GENERATOR_SU_V2_H */

