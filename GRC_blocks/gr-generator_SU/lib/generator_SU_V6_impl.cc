/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "generator_SU_V6_impl.h"
#include <cmath>
#include <cstdio>
#include <ctime>
#include <gnuradio/io_signature.h>
#include <assert.h>
#include <boost/random.hpp>
#include <boost/random/beta_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

// Turn this on by commenting (false) and uncommenting (true), for full debug output
// #define DOYOUWANTODEBUG (true)
#define DOYOUWANTODEBUG (false)

// constant preamble = +1+1j
#define CONSTANT_COMPLEX_PREAMBLE (1 + 1j)

// constant alpha in the UCB index
#define UCB_CONSTANT_ALPHA (1)

// max size of the bool vector used to send our message
#define MAX_SIZE_OF_BOOL_VECTOR (32)


/** This function takes a 2D GPS position on Earth, and returns a small bool vector
 *  Example:
 *  $ position_to_binary(std::vector<double> {48.6516678, -2.0214016})
 *  { true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, false, false, false, false, true, true, false, true, false, true, false, true, false, false, true, true, false, false, false, false, true, false, true, false, false, true, true, false, true, true, false, true, false, false, true, false, false, false, false, true, true, false, false, false, true, false, true }
*/
std::vector<bool> position_to_binary(std::vector<double> xy_pos) {
    long offset = 10000000L;
    std::vector<bool> ret;

    // read the vector xy
    const double x_pos = xy_pos[0];
    // std::cout << "x_pos = " << x_pos << std::endl;  // DEBUG
    const double y_pos = xy_pos[1];
    // std::cout << "y_pos = " << y_pos << std::endl;  // DEBUG

    // build two integers x and y
    const long x = std::floor((x_pos + 90) * offset);
    // std::cout << "x = " << x << std::endl;  // DEBUG
    const long y = std::floor((y_pos + 180) * offset);
    // std::cout << "y = " << y << std::endl;  // DEBUG

    // build a large integer xy
    long xy = (180L * offset) * y + x;
    // std::cout << "xy = " << xy << std::endl;  // DEBUG

    // read every bit of the integer
    while(xy) {
        ret.push_back(xy&1);
        xy >>= 1;
    }
    std::reverse(ret.begin(), ret.end());
    return ret;
}


/** This function takes a small string message, and returns a small bool vector
 *  Example:
 *  $ string_to_binary("#ICT2018")
 *  { false, true, true, true, false, false, false, false, true, true, false, false, false, true, false, true, true, false, false, false, false, false, true, true, false, false, true, false, true, false, true, false, true, false, false, true, false, false, false, false, true, true, true, false, false, true, false, false, true, false, true, false, false, false, true, true }
*/
std::vector<bool> string_to_binary(std::string message) {
    std::vector<bool> ret;
    int count_bits;
    char working_c;
    for(std::string::size_type i = 0; i < message.size(); i++) {
        working_c = message[i];
        // std::cout << "message[i] = " << message[i] << " for i = " << i << std::endl; // DEBUG

        count_bits = 0;
        while (working_c) {
            count_bits += 1;
            // std::cout << "working_c&1 = " << (int (working_c&1)) << " for working_c = " << working_c << " as int = " << (int (working_c)) << std::endl; // DEBUG
            ret.push_back(working_c&1);
            working_c >>= 1;
        }
        while(count_bits < 7) {
            // std::cout << "forcing bit = 0" << std::endl; // DEBUG
            ret.push_back(0);
            count_bits += 1;
        }
    }
    std::reverse(ret.begin(), ret.end());
    return ret;
}

// to print a bool
std::string bool_to_string(bool b) {
    return b ? "true" : "false";
}


/** Fake function that could take message from the Internet, a API, a captor or anything.
*/
#define FAKE_CONSTANT_MESSAGE ("ICT")

std::string get_new_message() {
    return FAKE_CONSTANT_MESSAGE;
}


#define FILEPATH_TO_SAVE_DATA_1 ("generator_SU_V6_impl_cc_data_file_1.txt")
#define FILEPATH_TO_SAVE_DATA_1_APPEND ("generator_SU_V6_impl_cc_data_file_1_append.txt")
#define FILEPATH_TO_SAVE_DATA_2 ("generator_SU_V6_impl_cc_data_file_2.txt")
#define FILEPATH_TO_SAVE_DATA_2_APPEND ("generator_SU_V6_impl_cc_data_file_2_append.txt")
#define FILEPATH_TO_SAVE_DATA_3 ("generator_SU_V6_impl_cc_data_file_3.txt")
#define FILEPATH_TO_SAVE_DATA_3_APPEND ("generator_SU_V6_impl_cc_data_file_3_append.txt")

void save_data(
    const char *filepath_to_save_data,
    bool append,
    std::vector<int> d_activeChannels,
    int d_activeChannelsNum,
    gr_complex *NbTrials,
    gr_complex *NbSuccess,
    gr_complex *UCBIndexes,
    gr_complex *SuccessRate
) {
    std::FILE* file = std::fopen(filepath_to_save_data, append ? "a" : "w");
    // erase file!
    printf("Saving data to file '%s'...\n", filepath_to_save_data);
    int activeChannel = d_activeChannels[0];

    // order of line is important!
    activeChannel = d_activeChannels[0];
    printf("%i", (int) real(NbTrials[activeChannel]));
    fprintf(file, "%i", (int) real(NbTrials[activeChannel]));
    for (int activeChannelId = 1; activeChannelId < d_activeChannelsNum; activeChannelId++){
        activeChannel = d_activeChannels[activeChannelId];
        printf(":%i", (int) real(NbTrials[activeChannel]));
        fprintf(file, ":%i", (int) real(NbTrials[activeChannel]));
    }
    if (append){
        fprintf(file, "    ");
    } else {
        printf("\n");
        fprintf(file, "\n");
    }

    // order of line is important!
    activeChannel = d_activeChannels[0];
    printf("%i", (int) real(NbSuccess[activeChannel]));
    fprintf(file, "%i", (int) real(NbSuccess[activeChannel]));
    for (int activeChannelId = 1; activeChannelId < d_activeChannelsNum; activeChannelId++){
        activeChannel = d_activeChannels[activeChannelId];
        printf(":%i", (int) real(NbSuccess[activeChannel]));
        fprintf(file, ":%i", (int) real(NbSuccess[activeChannel]));
    }
    printf("\n");
    fprintf(file, "\n");

    if (append) {
        std::fclose(file);
        return;
    }

    // order of line is important!
    activeChannel = d_activeChannels[0];
    printf("%g", (float) real(UCBIndexes[activeChannel]));
    fprintf(file, "%g", (float) real(UCBIndexes[activeChannel]));
    for (int activeChannelId = 1; activeChannelId < d_activeChannelsNum; activeChannelId++){
        activeChannel = d_activeChannels[activeChannelId];
        printf(":%g", (float) real(UCBIndexes[activeChannel]));
        fprintf(file, ":%g", (float) real(UCBIndexes[activeChannel]));
    }
    printf("\n");
    fprintf(file, "\n");

    // order of line is important!
    activeChannel = d_activeChannels[0];
    printf("%g", (float) real(SuccessRate[activeChannel]));
    fprintf(file, "%g", (float) real(SuccessRate[activeChannel]));
    for (int activeChannelId = 1; activeChannelId < d_activeChannelsNum; activeChannelId++){
        activeChannel = d_activeChannels[activeChannelId];
        printf(":%g", (float) real(SuccessRate[activeChannel]));
        fprintf(file, ":%g", (float) real(SuccessRate[activeChannel]));
    }
    printf("\n");
    fprintf(file, "\n");

    std::fclose(file);
}


namespace gr {
    namespace generator_SU {

        generator_SU_V6::sptr
        generator_SU_V6::make(
            const int Nb_ch,
            const std::vector<int> Active_ch,
            const int Preamb_l,
            const int data_l,
            const int delay_between_packets,
            const float r_data,
            const float i_data,
            const bool is_uniform,
            const bool is_thompson,
            const std::string const_message
        ) {
            return gnuradio::get_initial_sptr(
                new generator_SU_V6_impl(
                    Nb_ch,
                    Active_ch,
                    Preamb_l,
                    data_l,
                    delay_between_packets,
                    r_data,
                    i_data,
                    is_uniform,
                    is_thompson,
                    const_message
                )
            );
        }

        /*
        * The private constructor
        */
        generator_SU_V6_impl::generator_SU_V6_impl(
            const int Nb_ch,
            const std::vector<int> Active_ch,
            const int Preamb_l,
            const int data_l,
            const int delay_between_packets,
            const float r_data,
            const float i_data,
            const bool is_uniform,
            const bool is_thompson,
            const std::string const_message
        )
            : gr::sync_block(
                "generator_SU_V6",
                gr::io_signature::make(1, 1, Nb_ch * sizeof(int)),
                gr::io_signature::make(5, 5, Nb_ch * sizeof(gr_complex))
            ),
            d_Nb_ch(Nb_ch),
            d_activeChannels(Active_ch),
            d_activeChannelsNum(Active_ch.size()),
            d_preamLength(Preamb_l),
            d_dataLength(data_l),
            d_delay_between_packets(delay_between_packets),
            d_r_data(r_data),
            d_i_data(i_data),
            d_is_uniform(is_uniform),
            d_is_thompson(is_thompson),
            d_active_ch(0),
            d_time_count_global_clock(0),
            d_time_UCB(0),
            d_sum_reward(new int[Active_ch.size()]),
            d_nb_selection(new int[Active_ch.size()]),
            d_i_printed_about_this_message(false),
            d_message(const_message),
            d_bool_vector_to_send(new bool[MAX_SIZE_OF_BOOL_VECTOR]),
            d_bool_vector_offset(-1),
            d_all_UCB_index(new float[Active_ch.size()]),
            d_filepath_to_save_data(is_uniform ? FILEPATH_TO_SAVE_DATA_2 : (is_thompson ? FILEPATH_TO_SAVE_DATA_3 : FILEPATH_TO_SAVE_DATA_1)),
            d_filepath_to_save_data_append(is_uniform ? FILEPATH_TO_SAVE_DATA_2_APPEND : (is_thompson ? FILEPATH_TO_SAVE_DATA_3_APPEND : FILEPATH_TO_SAVE_DATA_1_APPEND))
        {
            // set_output_multiple(128);  // FIXME

            std::cout << std::endl << "Creating a new 'generator_SU_V6_impl.cc' block..." << std::endl;  // DEBUG
            int output_mutiple = sizeof(gr_complex) * d_Nb_ch;
            std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
            set_output_multiple( output_mutiple );

            // DEBUG show input values of parameters
            std::cout << "d_Nb_ch: " << d_Nb_ch << std::endl;  // DEBUG
            std::cout << "d_activeChannelsNum: " << d_activeChannelsNum << std::endl;  // DEBUG
            for (int index_of_channel = 0; index_of_channel < d_activeChannelsNum; index_of_channel++) {
                std::cout << "    d_activeChannels[" << index_of_channel << "] = " << d_activeChannels[index_of_channel] << std::endl;  // DEBUG
            }
            std::cout << "d_preamLength: " << d_preamLength << std::endl;  // DEBUG
            std::cout << "d_dataLength: " << d_dataLength << std::endl;  // DEBUG

            std::cout << "d_r_data: " << d_r_data << std::endl;  // DEBUG
            std::cout << "d_i_data: " << d_i_data << std::endl;  // DEBUG
            std::cout << "d_is_uniform: " << d_is_uniform << std::endl;  // DEBUG
            std::cout << "d_is_thompson: " << d_is_thompson << std::endl;  // DEBUG

            for (int j = 0; j < d_activeChannelsNum; j++) {
                d_sum_reward[j]    = 0;
                d_nb_selection[j]  = 0;
                d_all_UCB_index[j] = 0.0;
            }
            d_nb_selection[0] = 1;  // first selection will be on channel 1

            d_rng = new gr::random(((int) time(NULL)), 0, d_activeChannelsNum);

            // get and store the bool vector used to send data
            // std::string message = get_new_message();
            std::string message = d_message;
            std::cout << std::endl << "Using the constant message = " << message << std::endl;

            std::vector<bool> binary_message = string_to_binary(message);
            assert(binary_message.size() <= MAX_SIZE_OF_BOOL_VECTOR);

            // initialize the bool vector to full true (true = use default symbol = r_data + i_data * 1j)
            for (int i = 0; i < MAX_SIZE_OF_BOOL_VECTOR; i++) {
                d_bool_vector_to_send[i] = true;
            }
            // // FIXME to test, I only include ONE bit swap (2 detected)
            // d_bool_vector_to_send[MAX_SIZE_OF_BOOL_VECTOR / 2] = false;
            // first "bit" is the symbol; as its the ID of this user
            for (int i = 0; i < binary_message.size(); i++) {
                d_bool_vector_to_send[i + 1] = binary_message[i];
                std::cout << "    d_bool_vector_to_send[" << (i + 1) << "] = " << d_bool_vector_to_send[i + 1] << std::endl;  // DEBUG
            }

            std::cout << "d_filepath_to_save_data: " << d_filepath_to_save_data << std::endl;  // DEBUG
            std::cout << "d_filepath_to_save_data_append: " << d_filepath_to_save_data_append << std::endl;  // DEBUG
        }

        /*
        * Our virtual destructor.
        */
        generator_SU_V6_impl::~generator_SU_V6_impl() {
            delete d_rng;
        }

        int generator_SU_V6_impl::random_channel() {
            return d_rng->ran_int();
        }

        /*
        * Signal processing
        */
        int generator_SU_V6_impl::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            // std::cout << "Start generator_SU_V6_impl::work()..." << std::endl;

            const int *in = (const int *)input_items[0];
            gr_complex *Traffic     = (gr_complex *)output_items[0];
            gr_complex *NbTrials    = (gr_complex *)output_items[1];
            gr_complex *NbSuccess   = (gr_complex *)output_items[2];
            gr_complex *UCBIndexes  = (gr_complex *)output_items[3];
            gr_complex *SuccessRate = (gr_complex *)output_items[4];

            // Init parameters
            float UCB_index        = 1.0;
            float max_index        = 0.0;
            float mean             = 0.0;
            float exploration_bias = 0.0;
            int   offset           = 0;
            int   offset_bis       = 0;

            // we force everything to be 0
            for (int i = 0; i < noutput_items; i++) {
                for (int non_active_ch = 0; non_active_ch < d_Nb_ch; non_active_ch++) {
                    offset_bis = i * (d_Nb_ch) + non_active_ch;
                    Traffic[offset_bis]     = 0;
                    // NbTrials[offset_bis]    = 0;
                    // NbSuccess[offset_bis]   = 0;
                    // UCBIndexes[offset_bis]  = 0;
                    // SuccessRate[offset_bis] = 0;
                }
            }

            // now we work
            for (int i = 0; i < noutput_items; i++) {
                // we are only listening on the d_active_ch channel
                // and only sending data on this active channel
                offset = i * d_Nb_ch + d_activeChannels[d_active_ch];

                // first case, maybe we need to send data
                if ( d_time_count_global_clock < d_preamLength ) {  // we send the preamble
                    // constant value of the preamble
                    Traffic[offset] = CONSTANT_COMPLEX_PREAMBLE;
                }
                else
                if (d_time_count_global_clock < (d_preamLength + d_dataLength)) {
                    // we send the message
                    // FIXME c'est ici qu'on pourrait envoyer des bits d'informations
                    // FIXME we could split d_dataLength into chunks
                    // 1. get the chunk number
                    int chunk_number = ((int) floor((d_time_count_global_clock - d_preamLength) / d_preamLength));

                    // 2. we print when a new chunk is read
                    if (d_bool_vector_offset != chunk_number) {
                        d_bool_vector_offset = chunk_number;

                        if (DOYOUWANTODEBUG) {
                            std::cout << std::endl << "Currently at d_time_count_global_clock = " << d_time_count_global_clock << " and in a new chunk at position " << chunk_number << " so we use bit '" << bool_to_string(d_bool_vector_to_send[d_bool_vector_offset]) << "' and data = " << d_r_data << (d_bool_vector_to_send[d_bool_vector_offset] ? " + " : " - ") << d_i_data << std::endl;
                        }
                    }

                    // 2. get the bool from the d_bool_vector_offset
                    if (d_bool_vector_to_send[d_bool_vector_offset]) {
                        // 3. use the bool to send either r_data + i_data * 1j (true)
                        Traffic[offset] = (d_r_data + d_i_data * 1j);
                    } else {
                        // 3. or r_data - i_data * 1j (false)
                        Traffic[offset] = (d_r_data - d_i_data * 1j);
                    }


                    // DEBUG
                    // if ((DOYOUWANTODEBUG) && (i == 0)) {
                    if (d_i_printed_about_this_message == false) {
                        std::cout << std::endl << "Sending " << Traffic[offset] << " to " << d_active_ch << "th channel = " << d_activeChannels[d_active_ch] << "..." << std::endl;
                        d_i_printed_about_this_message = true;
                    }
                } else {
                    // during this period, a reward can be received
                    if ( in[offset] >= 1 ) {
                        d_sum_reward[d_active_ch] += 1;

                        // DEBUG
                        // if (DOYOUWANTODEBUG) {
                            std::cout << std::endl << "Receiving a reward = 1 from feedback message = " << ((int) in[offset]) << " on the " << d_active_ch << "th channel = " << d_activeChannels[d_active_ch] << std::endl;
                        // }
                    }
                }

                // here we wait until d_delay_between_packets
                if ( d_time_count_global_clock < (d_preamLength + d_dataLength + d_delay_between_packets - 1) ) {
                    d_time_count_global_clock += 1;
                } else {
                    // we reinitialize the count, because we take a decision RIGHT NOW
                    d_time_count_global_clock = 0;
                    d_i_printed_about_this_message = false;

                    if ( d_is_uniform ) {
                        d_active_ch = random_channel();
                        // if (DOYOUWANTODEBUG) {
                            std::cout << "Selecting channel #" << d_active_ch << " uniformly at random..." << std::endl;
                        // }
                    } else {
                        // UCB1 or TS algorithm
                        if ( (d_time_UCB + 1) < d_activeChannelsNum) {
                            // Initialization: pick each channel sequentially
                            // XXX the +1 is here to not select channel #0 twice in the initialization
                            d_active_ch = (d_time_UCB + 1) % d_activeChannelsNum;
                            // if (DOYOUWANTODEBUG) {
                                std::cout << "Selecting channel #" << d_active_ch << " for the initialization of UCB1..." << std::endl;
                            // }
                        } else {
                            // now, using UCB1 or TS algorithm to select the next active channel
                            for (int j = 0; j < d_activeChannelsNum; j++) {

                                // TS algorithm
                                if ( d_is_thompson ) {
                                    // build the beta distribution
                                    // Cf. https://www.boost.org/doc/libs/1_67_0/doc/html/boost_random/reference.html
                                    // Cf. https://www.boost.org/doc/libs/1_67_0/doc/html/boost/random/beta_distribution.html
                                    boost::random::beta_distribution<> a_beta_distribution(
                                        (float) 1 + d_sum_reward[j],
                                        (float) 1 + d_nb_selection[j] - d_sum_reward[j]
                                    );
                                    // get a sample from it
                                    boost::random::mt19937 gen;
                                    UCB_index = a_beta_distribution(gen);

                                    // DEBUG
                                    if (DOYOUWANTODEBUG) {
                                        std::cout << "Channel #" << j << " created a beta distribution with parameter a = " << 1 + d_sum_reward[j] << ", and b = " << 1 + d_nb_selection[j] - d_sum_reward[j] << ", and a random sample gave index = " << UCB_index << " ..." << std::endl;
                                    }
                                    // destroy the beta distribution? no need (don't care if it's slow)
                                } else {
                                    // UCB1 algorithm
                                    mean = ((float) d_sum_reward[j]) / ((float) d_nb_selection[j]);
                                    exploration_bias = std::sqrt(UCB_CONSTANT_ALPHA * std::log(1 + d_time_UCB) / ((float) (1 + d_nb_selection[j])));
                                    UCB_index = mean + exploration_bias;

                                    // DEBUG
                                    if (DOYOUWANTODEBUG) {
                                        std::cout << "Channel #" << j << " has mean = " << mean << ", and exploration bias = " << exploration_bias << ", so it has UCB index = " << UCB_index << " ..." << std::endl;
                                    }
                                }
                                d_all_UCB_index[j] = UCB_index;

                                // In the loop, keep the index of the arm with max UCB index
                                if (UCB_index >= max_index) {
                                    max_index = UCB_index;
                                    d_active_ch = j;
                                }
                            }

                            // DEBUG
                            // if (DOYOUWANTODEBUG) {
                                std::cout << "Channel #" << d_active_ch << " (= " << d_activeChannels[d_active_ch] << ") has the maximal UCB index = " << max_index << " and sum rewards = " << d_sum_reward[d_active_ch] << " and nb selection = " << d_nb_selection[d_active_ch] << std::endl;
                            // }
                        }
                    }

                    // total number of selections of this channel
                    d_nb_selection[d_active_ch] += 1;
                    // total number of call to UCB algorithm
                    d_time_UCB += 1;
                    // we save the new data to plot when there is a new UCB decision
                    save_data(d_filepath_to_save_data, false, d_activeChannels, d_activeChannelsNum, NbTrials, NbSuccess, UCBIndexes, SuccessRate);
                    save_data(d_filepath_to_save_data_append, true, d_activeChannels, d_activeChannelsNum, NbTrials, NbSuccess, UCBIndexes, SuccessRate);
                }

                // if (i == 0) {
                    offset_bis = i * (d_Nb_ch) + d_activeChannels[d_active_ch];
                    // then output nb of trials, success, UCB index, success rate etc

                    NbTrials[offset_bis]  = (gr_complex) d_nb_selection[d_active_ch];
                    if ((DOYOUWANTODEBUG) && (i == 0)) { std::cout << "NbTrials[offset_bis] = " << NbTrials[offset_bis] << " with offset_bis = " << offset_bis << " as i = " << i << " and active channel is = " << d_activeChannels[d_active_ch] << std::endl; }  // DEBUG

                    NbSuccess[offset_bis] = (gr_complex) d_sum_reward[d_active_ch];
                    if ((DOYOUWANTODEBUG) && (i == 0)) { std::cout << "NbSuccess[offset_bis] = " << NbSuccess[offset_bis] << " with offset_bis = " << offset_bis << " as i = " << i << " and active channel is = " << d_activeChannels[d_active_ch] << std::endl; }  // DEBUG

                    if (d_nb_selection[d_active_ch] > 0) {
                        SuccessRate[offset_bis] = (gr_complex) d_sum_reward[d_active_ch] / ((float) d_nb_selection[d_active_ch]);
                        if ((DOYOUWANTODEBUG) && (i == 0)) { std::cout << "SuccessRate[offset_bis] = " << SuccessRate[offset_bis] << " with offset_bis = " << offset_bis << " as i = " << i << " and active channel is = " << d_activeChannels[d_active_ch] << std::endl; }  // DEBUG
                    }

                    for (int j = 0; j < d_activeChannelsNum; j++) {
                        offset_bis = i * (d_Nb_ch) + d_activeChannels[j];
                        UCBIndexes[offset_bis] = (gr_complex) d_all_UCB_index[j];
                        if ((DOYOUWANTODEBUG) && (i == 0)) { std::cout << "UCBIndexes[offset_bis] = " << UCBIndexes[offset_bis] << " with offset_bis = " << offset_bis << " as i = " << i << " and channel is = " << d_activeChannels[j] << std::endl; }  // DEBUG
                    }
                // }

                if (d_time_count_global_clock <= 1) {
                    // FIXME we save the new data, NOW because it was just updated!
                    save_data(d_filepath_to_save_data, false, d_activeChannels, d_activeChannelsNum, NbTrials, NbSuccess, UCBIndexes, SuccessRate);
                    save_data(d_filepath_to_save_data_append, true, d_activeChannels, d_activeChannelsNum, NbTrials, NbSuccess, UCBIndexes, SuccessRate);
                }
            }
            // Tell runtime system how many output items we produced.
            return noutput_items;
        }
    } /* namespace generator_SU */
} /* namespace gr */
