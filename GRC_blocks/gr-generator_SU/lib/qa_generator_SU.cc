/*
 * Copyright 2012 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

/*
 * This class gathers together all the test cases for the gr-filter
 * directory into a single test suite.  As you create new test cases,
 * add them here.
 */

#include "qa_generator_SU.h"
#include "qa_generator_SU_V1.h"
#include "qa_generator_SU_V2.h"
#include "qa_test_source_SU_V2.h"
#include "qa_generator_SU_V4.h"

CppUnit::TestSuite *
qa_generator_SU::suite()
{
  CppUnit::TestSuite *s = new CppUnit::TestSuite("generator_SU");
  s->addTest(gr::generator_SU::qa_generator_SU_V1::suite());
  s->addTest(gr::generator_SU::qa_generator_SU_V2::suite());
  s->addTest(gr::generator_SU::qa_test_source_SU_V2::suite());
  s->addTest(gr::generator_SU::qa_generator_SU_V4::suite());

  return s;
}
