/* -*- c++ -*- */

#define GENERATOR_SU_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "generator_SU_swig_doc.i"

%{
#include "generator_SU/generator_SU_V1.h"
#include "generator_SU/generator_SU_V2.h"
#include "generator_SU/test_source_SU_V2.h"
#include "generator_SU/generator_SU_V3.h"
#include "generator_SU/generator_SU_V4.h"
#include "generator_SU/generator_SU_V5.h"
#include "generator_SU/generator_SU_V6.h"
%}


%include "generator_SU/generator_SU_V1.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, generator_SU_V1);
%include "generator_SU/generator_SU_V2.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, generator_SU_V2);
%include "generator_SU/test_source_SU_V2.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, test_source_SU_V2);
%include "generator_SU/generator_SU_V3.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, generator_SU_V3);
%include "generator_SU/generator_SU_V4.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, generator_SU_V4);
%include "generator_SU/generator_SU_V5.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, generator_SU_V5);
%include "generator_SU/generator_SU_V6.h"
GR_SWIG_BLOCK_MAGIC2(generator_SU, generator_SU_V6);
