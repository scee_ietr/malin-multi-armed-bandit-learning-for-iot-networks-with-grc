# MALIN: *M*ulti-*A*rm bandit *L*earning for *Internet* of things *N*etworks with GRC
> SCEE / GNU Radio Companion.

This folder contains the designs of different components for our [GNU Radio](http://gnuradio.org/) demo:


- A "Dynamic Object", using UCB1 : [USRP_TX_SU__v1.grc](USRP_TX_SU__v1.grc) (and its [python script](USRP_TX_SU__v1.py)),
  ![Object1_UCB__UI.png](../docs/Object1_UCB__UI.png)
- A "Dynamic Object", using uniform access : [USRP_TX_SU__v1__obj2.grc](USRP_TX_SU__v1__obj2.grc) (and its [python script](USRP_TX_SU__v1__obj2.py)),
  ![Object2_UniformAccess__UI.png](../docs/Object2_UniformAccess__UI.png)
- An IoT "Base Station" : [USRP_RX_BTS__v1.grc](USRP_RX_BTS__v1.grc) (and its [python script](USRP_RX_BTS__v1.py)),
  ![Gateway__UI.png](../docs/Gateway__UI.png)
- An IoT "Traffic Generator" : [USRP_TX_NOISE__v1.grc](USRP_TX_NOISE__v1.grc) (and its [python script](USRP_TX_NOISE__v1.py)).
  ![Noise__UI.png](../docs/Noise__UI.png)

## Notes
- Use GNU Radio companion to generate the python script on your platform, instead of using the one provided here (your version of GNU radio or Qt can be different).
- By default, the dynamic user designs use an "ugly" visualisation block available in GNU Radio companion. [This file implements a pretty Qt UI visualisation tool](DynamicMplCanvas.py), by using [matplotlib](http://matplotlib.org) and [text files to save the data](generator_SU_V6_impl_cc_data_file_1.txt). See the instructions in [`small_change_to_do.py`](small_change_to_do.py) regarding what and where to change the two dynamic user Python files.

## Copyright
Copyright (C) 2017-2018  Rémi Bonnefoi, Lilian Besson

GPLv3 Licensed ([file LICENSE](../LICENSE)).
