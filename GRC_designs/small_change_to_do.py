# Small changes to do on Dynamic User python files
# First, at the end of all the 'import...' lines in the header of the file, add this:

# 1. in header
from DynamicMplCanvas import *

# Then, in the __init__ function, around line 200, look for a line that contains (4,1,1,1) or (4, 1, 1, 1)
# Comment the line with self.top_grid_layout.addWidget(...)
# and add these two lines instead:

# and inside
        static_mpl_canvas = MyDynamicMplCanvas4(self.top_widget, width=5, height=4, dpi=100, Nb_ch=Nb_ch, idnumber=idnumber, Active_ch=Active_ch)
        self.top_grid_layout.addWidget(static_mpl_canvas, 4, 1, 1, 1)



    # Note: to debug the Python files, you can
    # add these lines in the beginning of the python files

    # https://stackoverflow.com/a/2663863/
    def trace(frame, event, arg):
        if not frame.f_code.co_filename.startswith('/usr/'):
            print "PYTHON DEBUG: event of type '%s', at file:line '%s:%d' ..." % (event, frame.f_code.co_filename, frame.f_lineno)
        return trace

    sys.settrace(trace)