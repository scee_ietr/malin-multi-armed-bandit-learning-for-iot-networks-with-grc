# MALIN: *M*ulti-*A*rm bandit *L*earning for *Internet* of things *N*etworks with GRC
> SCEE / GNU Radio Companion.

This folder contains some documentation for our [GNU Radio](http://gnuradio.org/) demo entitled "MALIN: Multi-Arm bandit Learning for Internet of things Networks with GRC".

## Interface of each components of the demo

- A "Dynamic Object", using UCB1 : [USRP_TX_SU__v1.grc](../GRC_designs/USRP_TX_SU__v1.grc) (and its [python script](../GRC_designs/USRP_TX_SU__v1.py)),
  ![Object1_UCB.png](Object1_UCB.png)
  ![Object1_UCB__UI.png](Object1_UCB__UI.png)
- A "Dynamic Object", using uniform access : [USRP_TX_SU__v1__obj2.grc](../GRC_designs/USRP_TX_SU__v1__obj2.grc) (and its [python script](../GRC_designs/USRP_TX_SU__v1__obj2.py)),
  ![Object2_UniformAccess.png](Object2_UniformAccess.png)
  ![Object2_UniformAccess__UI.png](Object2_UniformAccess__UI.png)
- An IoT "Base Station" : [USRP_RX_BTS__v1.grc](../GRC_designs/USRP_RX_BTS__v1.grc) (and its [python script](../GRC_designs/USRP_RX_BTS__v1.py)),
  ![Gateway.png](Gateway.png)
  ![Gateway__UI.png](Gateway__UI.png)
- An IoT "Traffic Generator" : [USRP_TX_NOISE__v1.grc](../GRC_designs/USRP_TX_NOISE__v1.grc) (and its [python script](../GRC_designs/USRP_TX_NOISE__v1.py)).
  ![Noise.png](Noise.png)
  ![Noise__UI.png](Noise__UI.png)

## Interface of the demo

- With the traffic generator and the gateway, waiting for new objects to join the game:
  ![Gateway_Noise_Object1__UI_3.png](Gateway_Noise_Object1__UI_3.png)
- With object 1 (using UCB1) communicating efficiently with the gateway despite a lot of traffic in channels 1 and 2:
  ![Gateway_Noise_Object1__UI_2.png](Gateway_Noise_Object1__UI_2.png)
- Comparison of object 1 (right), using UCB1, and object 2 (left), using uniform access:
  ![Object1_and_Object2__UI.png](Object1_and_Object2__UI.png)

## Demonstration of the success rate improved by UCB1 algorithm

- Using uniform access, all channels are tried the same number of times:
  ![Object2_UniformAccess__SuccessRates.png](Object2_UniformAccess__SuccessRates.png)
- Using a learning algorithm allows to try more the channels which are the most free, and increase the throughput of the network:
  ![Object1_UCB__SuccessRates.png](Object1_UCB__SuccessRates.png)

## GNU Radio designs

## Copyright
Copyright (C) 2017-2018  Rémi Bonnefoi, Lilian Besson

GPLv3 Licensed ([file LICENSE](../LICENSE)).
