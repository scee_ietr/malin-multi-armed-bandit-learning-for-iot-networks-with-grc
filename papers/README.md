# MALIN: *M*ulti-*A*rm bandit *L*earning for *Internet* of things *N*etworks with GRC
> SCEE / GNU Radio Companion.

This folder contains the LaTeX code for the papers we wrote to present and advertise this demo.

## Copyright
Copyright (C) 2017-2018  Rémi Bonnefoi, Lilian Besson

GPLv3 Licensed ([file LICENSE](../LICENSE)).
