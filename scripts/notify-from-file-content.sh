#!/usr/bin/env bash
# Use notify-send to display a notification:
# $1 is a file, and its content is displayed after $*
#
# Example:
# $ rm -f /tmp/message.txt ; echo "Object #1 : 'I LOVE YOU'" > /tmp/message.txt
# $ notify-from-file-content.sh /tmp/message.txt "Gateway received this message:"

function usage() {
    echo -e "\nUsage: notify-from-file-content.sh FILE MESSAGE...
Use notify-send to display a notification, FILE is a file, and its content is displayed after MESSAGE
\nLicence: GPL v3\n"
}

filepath="$1"
shift

message=""
if [ -f "$filepath" ]; then
    message="$(cat "$filepath")"
    echo -e "\n$(date)\nReading message from '$filepath': new message is: '$message'..."
fi
if [ -z "$message" ]; then
    usage
    exit 1
fi

echo -e "Notifying for one second, with text: '$* $message'..."
notify-send --icon="icons/indicator-messages.png" --expire-time="1000" "$* ${message}"
